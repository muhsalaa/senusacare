/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 7AB3FF
 */
import React, { Component } from 'react';
import { BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
//import { Provider } from 'react-redux';
//import ReduxThunk from 'redux-thunk';
//import { createStore, applyMiddleware } from 'redux';
//import reducers from './Reducers';
import Router from './Router';
//import * as Session from './Helpers/session';

export default class App extends Component {
  componentDidMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
      if (Actions.currentScene == 'home') {
        BackHandler.exitApp();
        return true;
      }
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  render() {
  //const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return <Router />;
  }
}
