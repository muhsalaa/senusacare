import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';
import Footer from './Components/footer';

import BidanHome from './Components/Bidan/bidanHome';
import BidanAccount from './Components/Bidan/bidanAccount';
import OrderServiceBidan from './Components/Bidan/orderServiceBidan';
import KonfirmasiPesananBidan from './Components/Bidan/konfirmasiPesananBidan';

import HomeCareHome from './Components/HomeCare/homeCareHome';
import PemasanganAlat from './Components/HomeCare/pemasanganAlat';
import LayananKeperawatan from './Components/HomeCare/layananKeperawatan';
import LayananKebidanan from './Components/HomeCare/layananBidan';
import KonfirmasiPesananHomeCare from './Components/HomeCare/konfirmasiPesanan';
import PesanHomeCare from './Components/HomeCare/pesanHomeCare';

import DokterHome from './Components/Dokter/dokterHome';
import DokterChat from './Components/Dokter/dokterChat';
import ProfilDokter from './Components/Dokter/profilDokter';

import Consultant from './Components/Consultant/consultant';

import KonsultasiGizi from './Components/KonsultasiGizi/konsultasiGizi';
import DietKhusus from './Components/KonsultasiGizi/dietKhusus';
import JenisDiet from './Components/KonsultasiGizi/jenisDiet';
import MenuDiet from './Components/KonsultasiGizi/menuDiet';
import KonsultasiGiziChat from './Components/KonsultasiGizi/konsultasiGiziChat';
import KeranjangGizi from './Components/KonsultasiGizi/keranjangGizi';

import InfoTerkiniHome from './Components/InfoTerkini/infoTerkiniHome';
import InfoDetail from './Components/InfoTerkini/infoDetail';

import TrainingCenterHome from './Components/TrainingCenter/trainingCenter';
import TrainingCenterDetail from './Components/TrainingCenter/trainingCenterDetail';
import TrainingCenterOrder from './Components/TrainingCenter/trainingCenterOrder';


import LabHome from './Components/Laboratorium/labHome';
import LabDetail from './Components/Laboratorium/labDetail';
import KeranjangLab from './Components/Laboratorium/keranjangLab';

import StoreHome from './Components/Store/storeHome';
import KeranjangStore from './Components/Store/keranjangStore';
import KatalogBarang from './Components/Store/katalogBarang';

import Login from './Components/Profile/login';
import Registrasi from './Components/Profile/registrasi';

import PembayaranSukses from './Components/pembayaranSukses';
import OrderDetailOrder from './Components/Order/orderDetail';
import KonfirmasiPembayaranOrder from './Components/Order/konfirmasiPembayaranOrder';

const RouterComponent = () => {
	return (
		<Router 
			//navigationBarStyle={{ backgroundColor: '#ceefe4' }} 
			//titleStyle={{ fontWeight: 'normal', fontFamily: 'JosefinSans-Bold', fontSize: 16 }}
		>
			<Stack key="root">
				
					<Scene 
						key="home" 
						hideNavBar
						title='Hello, Senusa Care Here !'
						component={Footer}
						initial
					/>

					{
						// Order
					}
					<Scene key="orderDetailOrder" hideNavBar title='Order Detail Order' component={OrderDetailOrder} />
					<Scene key="konfirmasiPembayaranOrder" hideNavBar title='Konfirmasi PEmbayaran Order' component={KonfirmasiPembayaranOrder} />

					{
						// BIDAN stack
					}
					<Scene key="bidanHome" hideNavBar title='Bidan' component={BidanHome} />
					<Scene key="bidanAccount" hideNavBar title='Akun Bidan' component={BidanAccount} />
					<Scene key="orderServiceBidan" hideNavBar title='Order Service Bidan' component={OrderServiceBidan} />
					<Scene key="konfirmasiPesananBidan" hideNavBar title='Konfirmasi Pesanan Bidan' component={KonfirmasiPesananBidan} />
					{
						// HOMECARE stack
					}
					<Scene key="homeCareHome" hideNavBar title='homecare' component={HomeCareHome} />
					<Scene key="pemasanganAlat" hideNavBar title='Pemasangan Alat' component={PemasanganAlat} />
					<Scene key="layananKeperawatan" hideNavBar title='Layanan Keperawatan' component={LayananKeperawatan} />
					<Scene key="layananKebidanan" hideNavBar title='Layanan Kebidanan' component={LayananKebidanan} />
					<Scene key="konfirmasiPesananHomeCare" hideNavBar title='Konfirmasi Pesanan' component={KonfirmasiPesananHomeCare} />
					<Scene key="pesanHomeCare" hideNavBar title='Pesan Home Care Service' component={PesanHomeCare} />
					{
						// DOKTER stack
					}
					<Scene key="dokterHome" hideNavBar title='Dokter' component={DokterHome} />
					<Scene key="dokterChat" hideNavBar title='Dokter' component={DokterChat} />
					<Scene key="profilDokter" hideNavBar title='Dokter' component={ProfilDokter} />
					{
						// CONSULTANT stack
					}
					<Scene key="consultant" hideNavBar title='Consultant' component={Consultant} />
					{
						// KONSULTASI GIZI stack
					}
					<Scene key="konsultasiGizi" hideNavBar title='Konsultasi Gizi' component={KonsultasiGizi} />
					<Scene key="konsultasiGiziChat" hideNavBar title='Konsultasi Gizi' component={KonsultasiGiziChat} />
					<Scene key="dietKhusus" hideNavBar title='Diet Khusus' component={DietKhusus} />
					<Scene key="jenisDiet" hideNavBar title='Jenis Diet' component={JenisDiet} />
					<Scene key="menuDiet" hideNavBar title='Menu Diet' component={MenuDiet} />
					<Scene key="keranjangGizi" hideNavBar title='Keranjang Gizi' component={KeranjangGizi} />
					{
						//INFO TERKINI stack
					}
					<Scene key="infoTerkiniHome" hideNavBar title='Info Terkini' component={InfoTerkiniHome} />
					<Scene key="infoDetail" hideNavBar title='Info Detail' component={InfoDetail} />
					{
						// TRAINING CENTER stack
					}
					<Scene key="trainingCenterHome" hideNavBar title='Training Center' component={TrainingCenterHome} />
					<Scene key="trainingCenterDetail" hideNavBar title='Training Center Detail' component={TrainingCenterDetail} />
					<Scene key="trainingCenterOrder" hideNavBar title='Training Center Order' component={TrainingCenterOrder} />
					{
						// LABORATORIUM stack
					}
					<Scene key="labHome" hideNavBar title='Laboratorium' component={LabHome} />
					<Scene key="labDetail" hideNavBar title='Laboratorium' component={LabDetail} />
					<Scene key="keranjangLab" hideNavBar title='Keranjang Lab' component={KeranjangLab} />
					{
						// STORE stack
					}
					<Scene key="storeHome" hideNavBar title='Store' component={StoreHome} />
					<Scene key="katalogBarang" hideNavBar title='Katalog Barang' component={KatalogBarang} />
					<Scene key="keranjangStore" hideNavBar title='Keranjang Store' component={KeranjangStore} />
					{
						// LOGIN REGISTER stack
					}
					<Scene key="login" hideNavBar title='Login' component={Login} />
					<Scene key="registrasi" hideNavBar title='Registrasi' component={Registrasi} />
					{
						// COMMON stack
					}
					<Scene key="PembayaranSukses" hideNavBar title='Pembayaran Sukses' component={PembayaranSukses} />
			</Stack>
		</Router>
	);
};

export default RouterComponent;
/*sceneStyle={{ backgroundColor: 'skyblue' }}*/
