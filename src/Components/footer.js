/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, View, StatusBar, Image, TouchableOpacity } from 'react-native';

import { Homepage } from './homepage';
import { Profile } from './Profile/profilePage';
import { NoProfile } from './Profile/noProfile';
import { NoOrder } from './Order/noOrder';
import { OrderHome } from './Order/orderHome';
import { TitikTiga } from './titikTiga';

const init = {
  opt1: false,
  opt2: false,
  opt3: false,
  opt4: false,
};

export default class Footer extends Component {

  state = {
    opt1: true,
    opt2: false,
    opt3: false,
    opt4: false,
  }

  render() {
    const {
      buttonInactive,
      buttonActive,
      footer
    } = styles;

    return (
      <View style={styles.container}>
        <StatusBar
          backgroundColor="#5e95df"
          barStyle="light-content"
        />

        <View style={{ flex: 1 }}>
          { this.state.opt1 && <Homepage /> }
          { this.state.opt2 && <OrderHome /> }
          { this.state.opt3 && <NoProfile /> }
          { this.state.opt4 && <TitikTiga /> }
        </View>

        <View style={footer}>
          <TouchableOpacity 
            style={[buttonInactive, this.state.opt1 && buttonActive]}
            onPress={() => this.setState({ ...init, opt1: true })}
          >
            <Image
              style={{ width: 50, height: 50 }}
              source={require('../Assets/icon-footer/home-fill2.png')}
            />
          </TouchableOpacity>

          <TouchableOpacity 
            style={[buttonInactive, this.state.opt2 && buttonActive]}
            onPress={() => this.setState({ ...init, opt2: true })}
          >
            <Image
              style={{ width: 35, height: 30 }}
              source={require('../Assets/icon-footer/Group.png')}
            />
          </TouchableOpacity>
          
          <TouchableOpacity 
            style={[buttonInactive, this.state.opt3 && buttonActive]}
            onPress={() => this.setState({ ...init, opt3: true })}
          >
            <Image
              style={{ width: 30, height: 30 }}
              source={require('../Assets/icon-footer/home-fill23.png')}
            />
          </TouchableOpacity>
          
          <TouchableOpacity 
            style={[buttonInactive, this.state.opt4 && buttonActive]}
            onPress={() => this.setState({ ...init, opt4: true })}
          >
            <Image
              style={{ width: 35, height: 35 }}
              source={require('../Assets/icon-footer/home-fill22.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fefefe',
  },
  buttonActive: {
    borderBottomWidth: 3,
    borderColor: '#F88C8F',
    width: 50,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonInactive: {
    height: '100%',
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  footer: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    elevation: 15
  }
});
