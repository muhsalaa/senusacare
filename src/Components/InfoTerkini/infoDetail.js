import React, { Component } from 'react';
import { View, Text, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class InfoDetail extends Component {

	titleRender() {
		var x = this.props.judul;
		if (x){
			var y = x.split(' ').slice(0, 3).join(' ');
		}
		return y;
	}

	render() {
		const {
			header,
			textt,
			textt2,
			card
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>{this.titleRender()}</Text>
				</View>
				<ScrollView>
				<Image
					style={{ width: '100%', height: 250, position: 'absolute', top: 0 }}
          source={require('../../Assets/img1.jpg')}
				/>

				<View style={card}>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#231f20', marginBottom: 10 }}>{this.props.judul}</Text>
					<Text style={{ fontSize: 16, fontWeight: '700', marginBottom: 10 }}>{this.props.author}</Text>
					<View style={{ borderTopWidth: 1, borderColor: '#231f20', width: '100%', marginBottom: 10 }} />
					<Text style={{ fontSize: 14, textAlign: 'left' }}>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam laoreet, justo id ultricies vulputate, mi diam tincidunt nisi, eget aliquet ipsum urna eu urna. Etiam posuere nulla vel auctor blandit. Proin sed convallis leo. Duis sollicitudin velit sapien, ac vestibulum felis finibus a. Integer sodales malesuada iaculis. Phasellus ut velit at ipsum molestie congue in ac ante. Duis vulputate, purus ut ornare scelerisque, urna arcu commodo massa, ac tincidunt tellus justo et lectus. In diam dolor, posuere eu nisl a, ullamcorper posuere risus. Maecenas ac pulvinar nisi, at blandit leo.{'\n'}{'\n'}

						Vestibulum dapibus, diam vel vulputate scelerisque, velit purus efficitur nunc, vitae interdum nisi urna a justo. Sed eu posuere ipsum. Nulla eu erat vel elit ultricies eleifend. Duis at nisi vel tortor semper porta at a velit. Nam posuere arcu sit amet bibendum hendrerit. Nulla placerat nibh eget massa tempus, a tincidunt velit pellentesque. Donec fringilla est nisl. Integer a vulputate libero, scelerisque vehicula metus.{'\n'}{'\n'}

						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris placerat libero in neque convallis efficitur. Etiam facilisis diam sit amet massa vulputate, ac fringilla lorem varius. Vestibulum rhoncus orci eget tellus semper scelerisque. Sed leo elit, lacinia id euismod ac, mollis non nisi. Phasellus luctus malesuada sem quis faucibus. Nulla id blandit sapien, quis malesuada dui. Mauris sapien lorem, hendrerit eget leo eget, cursus dignissim dui. Sed pharetra lectus ante, eu aliquet velit luctus sit amet. Curabitur mollis mi nisi, ut auctor lectus luctus vel. Fusce in orci vitae dui porta pharetra. Nullam venenatis ac massa aliquet aliquam. Morbi sollicitudin bibendum arcu, laoreet vestibulum neque pretium in. Aenean quis viverra sapien.{'\n'}{'\n'}

						Integer ultrices arcu tortor. Aenean dictum turpis a arcu volutpat bibendum. Aenean ullamcorper ligula eget pulvinar consequat. Integer metus eros, tincidunt sed dui quis, hendrerit tristique lectus. Vestibulum nec est sed nisi ullamcorper imperdiet. Aenean ac urna massa. Fusce nulla lorem, tempus in semper et, vehicula ut quam.
					</Text>
				</View>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 16,
		fontWeight: '900',
		color: '#231f20'
	},
	textt2: {
		fontSize: 16,
		color: '#231f20'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		flex: 1,
		marginTop: 180, 
		width: width - 40,
		alignSelf: 'center', 
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		justifyContent: 'space-between'
	}
};
