import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class LabHome extends Component {

	render() {
		const {
			header,
			overlay,
			textt
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Laboratorium</Text>
				</View>
				<ScrollView>
				<View style={{ paddingHorizontal: 20, justifyContent: 'space-between', paddingTop: 20, flex: 1 }}>
						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() =>  Actions.labDetail({ judul: 'Cek Gula Darah' })}>
							<Image
								style={{ width: '100%', height: 100, borderRadius: 10 }}
								source={require('../../Assets/img1.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>CEK GULA DARAH</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() =>  Actions.labDetail({ judul: 'Cek Kolesterol' })}>
							<Image
								style={{ width: '100%', height: 100, borderRadius: 10 }}
								source={require('../../Assets/img2.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>CEK KOLESTEROL</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() =>  Actions.labDetail({ judul: 'Cek Asam Urat' })}>
							<Image
								style={{ width: '100%', height: 100, borderRadius: 10 }}
								source={require('../../Assets/img3.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>CEK ASAM URAT</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() =>  Actions.labDetail({ judul: 'Cek Darah Lengkap' })}>
							<Image
								style={{ width: '100%', height: 100, borderRadius: 10 }}
								source={require('../../Assets/sayur.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>CEK DARAH LENGKAP</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() =>  {}}>
							<Image
								style={{ width: '100%', height: 100, borderRadius: 10 }}
								source={require('../../Assets/sayur2.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>LAINYA</Text>
							</View>
						</TouchableOpacity>
						
				</View>
				</ScrollView>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 20,
		color: '#ffffff',
		fontWeight: '900',
		textAlign: 'center'
	},
	overlay: { 
		position: 'absolute', 
		top: 0, 
		bottom: 0, 
		left: 0, 
		right: 0, 
		backgroundColor: 'rgba(0,0,0,0.3)', 
		borderRadius: 10, 
		justifyContent: 'center', 
		alignItems: 'center',
		paddingHorizontal: 20
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	}
};
