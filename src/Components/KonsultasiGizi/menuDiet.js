import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class MenuDetail extends Component {

	render() {
		const {
			header,
			header2,
			header3,
			textt,
			food,
			orderButton,
			card,
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>{this.props.judul}</Text>
				</View>
				<View style={header2}>
					<Text style={textt}>Rp {this.props.price}</Text>
				</View>
				<View style={header3} />
				<ScrollView>
				<View style={{ flex: 1 }}>
					<View style={card}>
						<Image
							style={food}
							source={this.props.img}
						/>
						<Text style={{ fontSize: 14, fontWeight: '900', color: '#231f20', marginBottom: 10 }}>Komposisi</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 10 }}>1. 100gr Terigu</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 10 }}>2. Nasi 500gr</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 10 }}>3. Potongan Wortel 50gr</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 10 }}>4. Daging sapi non lemak 100gr</Text>
						<Text style={{ fontSize: 10, color: '#231f20', marginBottom: 10 }}>5. Peterseli</Text>
						<Text style={{ fontSize: 10, color: '#231f20' }}>6. Ikan Tuna asap Otaji</Text>
					</View>

					<TouchableOpacity style={orderButton} onPress={() => Actions.keranjangGizi({
						names: this.props.judul,
						price: this.props.price,
						img: this.props.img
					})}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BELI</Text>
					</TouchableOpacity>
				</View>
				</ScrollView>

			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 22, 
		fontWeight: '900', 
		color: '#ffffff',
		marginLeft: 30
	},
	textt2: {
		fontSize: 16,
		color: '#231f20'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	header2: {
		height: 60,
		flexDirection: 'row',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	header3: {
		height: 80,
		backgroundColor: '#7AB4FE',
		position: 'absolute',
		top: 140,
		left: 0,
		right: 0,
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		marginBottom: 25
	},
	food: {
		width: '100%',
		height: 200,
		borderRadius: 5,
		marginBottom: 10
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
};
