import React, { Component } from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class DietKhusus extends Component {

	render() {
		const {
			header,
			overlay,
			textt
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Diet Khusus</Text>
				</View>
				<ScrollView>
				<View style={{ paddingHorizontal: 20, justifyContent: 'space-between', paddingTop: 20, flex: 1 }}>
						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() => Actions.jenisDiet({ judul: 'Makanan Diet Diabetes' })}>
							<Image
								style={{ width: '100%', height: 150, borderRadius: 10 }}
								source={require('../../Assets/sayur4.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>DIET DIABETES</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity style={{ marginBottom: 20 }} onPress={() => Actions.jenisDiet({ judul: 'Makanan Diet Hipertensi' })}>
							<Image
								style={{ width: '100%', height: 150, borderRadius: 10 }}
								source={require('../../Assets/sayur3.jpg')}
							/>
							<View style={overlay}>
								<Text style={textt}>DIET HIPERTENSI</Text>
							</View>
						</TouchableOpacity>
						
				</View>
				</ScrollView>
			
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 20,
		color: '#ffffff',
		fontWeight: '900',
		textAlign: 'center'
	},
	overlay: { 
		position: 'absolute', 
		top: 0, 
		bottom: 0, 
		left: 0, 
		right: 0, 
		backgroundColor: 'rgba(0,0,0,0.2)', 
		borderRadius: 10, 
		justifyContent: 'center', 
		alignItems: 'center',
		paddingHorizontal: 20
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	}
};
