import React, { Component } from 'react';
import { View, Text, FlatList, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class Consultant extends Component {

	render() {
		const {
			header,
			card
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Consultant</Text>
				</View>
				
				<Image
					style={{ width: 252, height: 135, alignSelf: 'center', marginTop: 30, marginBottom: 30 }}
					source={require('../../Assets/icon-another/hacker.png')}
				/>

				<Text style={{ fontSize: 22, fontWeight: '900', color: '#231f20', marginLeft: 20, marginBottom: 20 }}>Lets Talk With Us</Text>
				
				<View style={card}>
					<Text style={{ fontSize: 14, color: '#231f20' }}>Weekday</Text>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#7AB4FE', marginBottom: 15 }}>08:00 am - 16:00 pm</Text>

					<Text style={{ fontSize: 14, color: '#231f20' }}>Weekend</Text>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#7AB4FE', marginBottom: 20 }}>08:00 am - 13:00 pm</Text>
					
					<Text style={{ fontSize: 14, color: '#231f20', marginBottom: 15 }}>Contact Us</Text>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<TouchableOpacity>
							<Image
								style={{ width: 28, height: 28, marginRight: 20 }}
								source={require('../../Assets/icon-another/close-envelope.png')}
							/>
						</TouchableOpacity>

						<TouchableOpacity>
							<Image
								style={{ width: 25, height: 25, marginRight: 20 }}
								source={require('../../Assets/icon-another/phone-receiver.png')}
							/>
						</TouchableOpacity>

						<TouchableOpacity>
							<Image
								style={{ width: 25, height: 25, marginRight: 20 }}
								source={require('../../Assets/icon-another/whatsapp.png')}
							/>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		height: 250, 
		width: width - 40,
		alignSelf: 'center', 
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5
	}
};
