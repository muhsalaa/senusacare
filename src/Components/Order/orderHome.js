import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const init = {
		opt1: false,
		opt2: false,
		opt3: false,
		opt4: false,
		opt5: false,
		opt6: false,
	};

export default class OrderHome extends Component {
	state = {
		opt1: true,
		opt2: false,
		opt3: false,
		opt4: false,
		opt5: false,
		opt6: false,
		bidan: [
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Obat Sehat',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ],
    dokter: [
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ],
    homeCare: [
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Obat Sehat',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Pil APTX-4869',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ],
    trainingCenter: [
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Obat Sehat',
        img: require('../../Assets/sayur2.jpg'),
        price: 'Rp 166.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Pil APTX-4869',
        img: require('../../Assets/sayur3.jpg'),
        price: 'Rp 66.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ],
    store: [
      {
        nama: 'Infus',
        img: require('../../Assets/sayur4.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Obat Sehat',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/sayur3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Infus',
        img: require('../../Assets/sayur.jpg'),
        price: 'Rp 166.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ],
    laboratorium: [
      {
        nama: 'Infus',
        img: require('../../Assets/mask3.jpg'),
        price: 'Rp 66.000',
        status: 'bayar',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
      {
        nama: 'Obat Sehat',
        img: require('../../Assets/mask2.jpg'),
        price: 'Rp 166.000',
        status: 'batal',
        alamat: 'Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan'
      },
    ]
	}

	renderData() {
		if (this.state.opt1) {
			return this.state.homeCare;
		} else if (this.state.opt2) {
			return this.state.dokter;
		} else if (this.state.opt3) {
			return this.state.trainingCenter;
		} else if (this.state.opt4) {
			return this.state.store;
		} else if (this.state.opt5) {
			return this.state.laboratorium;
		} else if (this.state.opt6) {
			return this.state.bidan;
		}
	}

	render() {
		const {
			header,
			button,
			bottomBorder,
			containerButton,
			card,
			text2,
			text1,
			labels,
			labelsBatal,
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Order</Text>
				</View>

				<View style={containerButton}>
					<TouchableOpacity style={this.state.opt1 && bottomBorder} onPress={() => this.setState({ ...init, opt1: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/blood.png')}
						/>
					</TouchableOpacity>
					<TouchableOpacity style={this.state.opt2 && bottomBorder} onPress={() => this.setState({ ...init, opt2: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/doctor.png')}
						/>
					</TouchableOpacity>
					<TouchableOpacity style={this.state.opt3 && bottomBorder} onPress={() => this.setState({ ...init, opt3: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/Group.png')}
						/>
					</TouchableOpacity>
					<TouchableOpacity style={this.state.opt4 && bottomBorder} onPress={() => this.setState({ ...init, opt4: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/shopping-bag.png')}
						/>
					</TouchableOpacity>
					<TouchableOpacity style={this.state.opt5 && bottomBorder} onPress={() => this.setState({ ...init, opt5: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/flask.png')}
						/>
					</TouchableOpacity>
					<TouchableOpacity style={this.state.opt6 && bottomBorder} onPress={() => this.setState({ ...init, opt6: true })}>
						<Image
							style={button}
							source={require('../../Assets/icon-homepage/nurse.png')}
						/>
					</TouchableOpacity>
				</View>

				<View style={{ flex: 1 }}>
          <FlatList
            data={this.renderData()}
            numColumns={1}
            contentContainerStyle={{ paddingTop: 15, paddingHorizontal: 20 }}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) =>
              <TouchableOpacity style={card} onPress={() => Actions.orderDetailOrder({
                names: item.nama,
                price: item.price,
                img: item.img,
                alamat: item.alamat
              })}>
                <Text style={text1}>
                  {item.nama} (&nbsp;
                    <Text style={text2}>
                      {item.price}
                    </Text>
                  &nbsp;)
                </Text>

                <View style={{ marginTop: 10, flexDirection: 'row' }}>
                  <Image
                    style={{ width: 60, height: 60, borderRadius: 5, marginRight: 10 }}
                    source={item.img}
                  />
                  <Text style={{ fontSize: 10, flex: 1 }}>
                    {
                       item.alamat.slice(0, 38) + '...'
                    }
                  </Text>
                  <View style={{ width: 80, justifyContent: 'flex-end' }}>
                    <View style={item.status == 'bayar' ? labels : labelsBatal}>
                      <Text style={{ fontSize: 10, color: '#ffffff' }}>{item.status == 'bayar' ? 'BAYAR' : 'BATAL'}</Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            }
          />
          
        </View>
				
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	button: { 
		width: 30, 
		height: 30, 
	},
	bottomBorder: {
		borderBottomWidth: 3,
		borderColor: '#F88C8F',
		height: '100%',
		justifyContent: 'center'
	},
	containerButton: { 
		height: 60, 
		backgroundColor: '#ffffff',
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		elevation: 3
	},
	card: {
    padding: 10,
    width: '100%',
    alignSelf: 'center',
    backgroundColor: '#ffffff',
    elevation: 5,
    marginBottom: 15,
  },
  text1: { 
    fontSize: 14, 
    fontWeight: '600', 
    color: '#231f20' 
  },
  text2: { 
    fontSize: 14, 
    fontWeight: '600', 
    color: '#7AB4FE' 
  },
  labels: { 
    borderRadius: 5, 
    backgroundColor: '#F88C8F',
    alignItems: 'center', 
    justifyContent: 'center', 
    width: '100%', 
    height: 25 
  },
  labelsBatal: { 
    borderRadius: 5, 
    backgroundColor: '#808080',
    alignItems: 'center', 
    justifyContent: 'center', 
    width: '100%', 
    height: 25 
  },
};

export { OrderHome };
