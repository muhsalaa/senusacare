import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class NoOrder extends Component {

	render() {
		const {
			header,
			orderButton,
			textt2,
			textt
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Order</Text>
				</View>
				
				<View style={{ paddingHorizontal: 20, justifyContent: 'space-around', flex: 1 }}>
					<Image
						style={{ width: 272, height: 165, alignSelf: 'center' }}
						source={require('../../Assets/icon-another/boxman.png')}
					/>

					<View>
						<Text style={textt}>Belum Ada Pesanan</Text>
						<Text style={textt2}>Silahkan Masuk atau daftar untuk melakukan pemesanan</Text>
					</View>

					<TouchableOpacity style={orderButton} onPress={() => Actions.login()}>
						<Text style={{ fontSize: 16, color: '#ffffff', fontWeight: '900' }}>Masuk/ Daftar</Text>
					</TouchableOpacity>
				</View>
				
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		
	},
	textt: { 
		fontSize: 24, 
		fontWeight: '900', 
		color: '#231f20', 
		alignSelf: 'center', 
		marginBottom: 5
	},
	textt2: {
		fontSize: 16, 
		fontWeight: '900', 
		alignSelf: 'center', 
		textAlign: 'center'
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	orderButton: { 
		alignItems: 'center', 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 20, 
		borderRadius: 3,
		height: 50
	},
};

export { NoOrder };
