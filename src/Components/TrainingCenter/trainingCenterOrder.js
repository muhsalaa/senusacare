import React, { Component } from 'react';
import { View, Text, Image, 
	Dimensions, ScrollView, TouchableOpacity, 
	TouchableWithoutFeedback, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';
//import MapView from 'react-native-maps';

const width = Dimensions.get('window').width;

export default class TrainingCenterOrder extends Component {
	state = {
		cash: true,
		transfer: false,
		qty: 1,
		price: 45000
	}

	priceTotal() {
		let harga = this.state.qty * this.state.price;
		let hargaDenganTitik = 'Rp ' + harga.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
		return hargaDenganTitik;
	}

	render() {
		const {
			header,
			header2,
			textt,
			textt2,
			card,
			iconStyle,
			orderButton,
			texttt,
			totalTextt,
			dotActive,
			dotInactive,
			buttonPlusMin,
			inputStyle,
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Konfirmasi Pesanan</Text>
				</View>

				<View style={header2}/>

				<ScrollView>
					<View style={card}>
						<Text style={{ ...textt2, fontWeight: '900' }}>TIKET</Text>
						<Text style={{ ...textt, marginBottom: 15, fontWeight: '500' }}>Pelatihan Golden Treatment</Text>

						<View style={{ flexDirection: 'row', marginBottom: 15 }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/calendar.png')}
							/>
							<Text style={textt}>Sabtu, 20 Desember 2018{'\n'}09:00 WIB</Text>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 15 }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/pin.png')}
							/>
							<Text style={textt}>Jl. sempati komplek asabri no 66 rt 45 rw 09 landasan ulin banjarbaru kalimantan selatan</Text>
						</View>

						<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
							<Image
								style={iconStyle}
								source={require('../../Assets/icon-another/pricetag.png')}
							/>
							<Text style={textt}>{this.priceTotal()}</Text>
						</View>

						<View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginLeft: 30 }}>
							<Text style={textt}>Jumlah Tiket</Text>
							<View style={{ flexDirection: 'row', alignItems: 'center' }}>
								<TouchableOpacity 
									style={buttonPlusMin} 
									onPress={() => {
										if (this.state.qty != 1) {
											this.setState({ qty: this.state.qty - 1 });
										}
									}}
								>
									<Icon 
										name="minus"
										size={10}
										color="#231f20"
									/>
								</TouchableOpacity>
								<Text style={{ fontSize: 12, color: '#231f20', fontWeight: '600', paddingHorizontal: 10 }}>{this.state.qty}</Text>
								<TouchableOpacity style={buttonPlusMin} onPress={() => this.setState({ qty: this.state.qty + 1 })}>
									<Icon 
										name="plus"
										size={10}
										color="#231f20"
									/>
								</TouchableOpacity>
							</View>
						</View>
					</View>

					<View style={{ ...card, marginTop: 20 }}>
						<Text style={textt2}>Detail Pembayaran</Text>
						<TextInput 
							style={inputStyle}
							placeholder='Pekerjaan'
							placeholderTextColor='#231f20'
							underlineColorAndroid='transparent'
						/>

						<TextInput 
							style={inputStyle}
							placeholder='Nama Lengkap'
							placeholderTextColor='#231f20'
							underlineColorAndroid='transparent'
						/>

						<TextInput 
							style={inputStyle}
							placeholder='Email'
							placeholderTextColor='#231f20'
							underlineColorAndroid='transparent'
						/>

						<TextInput 
							style={inputStyle}
							placeholder='No HP'
							placeholderTextColor='#231f20'
							keyboardType='numeric'
							underlineColorAndroid='transparent'
						/>

						<TextInput 
							style={inputStyle}
							placeholder='ID Card (KTP/Other)'
							placeholderTextColor='#231f20'
							keyboardType='numeric'
							underlineColorAndroid='transparent'
						/>
					</View>

					{
						//Second card, Detail Pembayaran
					}
					<View style={{ ...card, marginTop: 20, marginBottom: 20 }}>
						<Text style={textt2}>Detail Pembayaran</Text>
					
						<TouchableWithoutFeedback onPress={() => this.setState({ cash: true, transfer: false })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.cash && dotActive]} />
								<Text style={textt}>Bayar Tunai Ditempat</Text>
							</View>
						</TouchableWithoutFeedback>


						<TouchableWithoutFeedback onPress={() => this.setState({ cash: false, transfer: true })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.transfer && dotActive]} />
								<Text style={textt}>Bank Transfer</Text>
							</View>
						</TouchableWithoutFeedback>

						<View style={{ flexDirection: 'row', marginLeft: 24, marginBottom: 15 }}>
							<Image
								style={{ height: 40, width: 80, marginRight: 10 }}
								source={require('../../Assets/icon-another/logo-bni.png')}
							/>

							<View style={{ flex: 1 }}>
								<Text style={{ fontSize: 12, color: '#231f20', lineHeight: 12 }}>Transfer Ke:</Text> 
								<Text style={{ fontSize: 12, color: '#7AB4FE' }}>0741325741</Text> 
								<Text style={{ fontSize: 10, color: '#231f20' }}>A/n yayasan sosial teknologi nusantara</Text> 
							</View>
						</View>

						<View style={totalTextt}>
							<Text style={{ ...texttt, fontWeight: '300' }}>Kode Unik</Text>
							<Text style={{ ...texttt, fontWeight: '300' }}>Rp 22</Text>
						</View>

						<View style={totalTextt}>
							<Text style={texttt}>Total Pembayaran</Text>
							<Text style={texttt}>Rp 300.022</Text>
						</View>
					</View>

					<TouchableOpacity style={orderButton}>
						<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BAYAR</Text>
					</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 12, 
		lineHeight: 12, 
		flex: 1, 
		color: '#231f20' 
	},
	textt2: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20',
		marginBottom: 10
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center', 
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		justifyContent: 'space-between'
	},
	iconStyle: { 
		height: 20, 
		width: 20, 
		marginRight: 10 
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
	texttt: { 
		fontSize: 12, 
		color: '#231f20',
		fontWeight: '500'
	},
	totalTextt: { 
		flexDirection: 'row', 
		marginBottom: 15, 
		justifyContent: 'space-between',
		marginLeft: 24
	},
	dotInactive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10
	},
	dotActive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10,
		backgroundColor: '#F88C8F'
	},
	header2: {
		position: 'absolute',
		top: 80,
		left: 0,
		right: 0,
		backgroundColor: '#7AB4FE',
		height: 80
	},
	buttonPlusMin: { 
		width: 25, 
		height: 25, 
		justifyContent: 'center', 
		alignItems: 'center',
		borderWidth: 1,
		borderColor: '#231f20',
		borderRadius: 5
	},
	inputStyle: {
		width: '100%',
		height: 40,
		marginBottom: 10,
		color: '#231f20',
		backgroundColor: '#e9e9e9',
		borderRadius: 5,
		fontSize: 12,
		paddingHorizontal: 10
	},
};
