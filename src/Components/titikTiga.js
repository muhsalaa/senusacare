import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class TitikTiga extends Component {

	render() {
		const {
			textt,
			kotak,
			margen,
			ikon
		} = styles;

		return (
			<View style={styles.container}>
				<Text style={textt}>AKUN</Text>
				{
					// Main
				}
				<TouchableOpacity style={kotak}>
					<Image
						style={{ width: 30, height: 30, marginRight: 10 }}
						source={require('../Assets/on.png')}
					/>
					<Text style={textt}>Masuk</Text>
					<View style={ikon}>
						<Icon 
							name="angle-right"
							size={20}
							color="#231f20"
					  />
				  </View>
				</TouchableOpacity>

				<View style={margen} />

				<Text style={textt}>TENTANG</Text>
				{
					// Tentang
				}
				<TouchableOpacity style={kotak}>
					<Image
						style={{ width: 30, height: 30, marginRight: 10 }}
						source={require('../Assets/icon-footer/home-fill2.png')}
					/>
					<Text style={textt}>Tentang Senusa Care</Text>
					<View style={ikon}>
						<Icon 
							name="angle-right"
							size={20}
							color="#231f20"
					  />
				  </View>
				</TouchableOpacity>
				{
					// KEtenutan Layanan
				}
				<TouchableOpacity style={kotak}>
					<Image
						style={{ width: 30, height: 30, marginRight: 10 }}
						source={require('../Assets/docs.png')}
					/>
					<Text style={textt}>Ketentuan Layanan</Text>
					<View style={ikon}>
						<Icon 
							name="angle-right"
							size={20}
							color="#231f20"
					  />
				  </View>
				</TouchableOpacity>

				<View style={margen} />
				{
					// IG
				}
				<Text style={textt}>IKUTI KAMI</Text>
				<TouchableOpacity style={kotak}>
					<Image
						style={{ width: 30, height: 30, marginRight: 10 }}
						source={require('../Assets/ig.png')}
					/>
					<Text style={textt}>Follow Instagram Senusa Care</Text>
					<View style={ikon}>
						<Icon 
							name="angle-right"
							size={20}
							color="#231f20"
					  />
				  </View>
				</TouchableOpacity>
				{
					// playstore
				}
				<TouchableOpacity style={kotak}>
					<Image
						style={{ width: 30, height: 30, marginRight: 10 }}
						source={require('../Assets/ps.png')}
					/>
					<Text style={textt}>Nilai Kami di Playstore</Text>
					<View style={ikon}>
						<Icon 
							name="angle-right"
							size={20}
							color="#231f20"
					  />
				  </View>
				</TouchableOpacity>

				<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
					<Text style={textt}>Senusa Foundation</Text>
					<Text style={textt}>v1.0.0</Text>
				</View>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		paddingHorizontal: 20,
		paddingTop: 20
	},
	textt: {
		fontSize: 14,
		color: '#231f20'
	},
	kotak: { 
		borderBottomWidth: 1, 
		borderColor: '#A9A9A(', 
		width: '100%', 
		alignItems: 'center',
		flexDirection: 'row',
		paddingTop: 10,
		paddingBottom: 15
	},
	margen: {
		marginBottom: 20
	},
	ikon: { 
		justifyContent: 'flex-end', 
		flex: 1, 
		flexDirection: 'row' 
	}
};

export { TitikTiga };
