import React from 'react';
import { View, Text, ScrollView, Image, TouchableOpacity } from 'react-native';
import Swiper from 'react-native-swiper';
import { Actions } from 'react-native-router-flux';

const Homepage = () => {
	const {
		card,
		cardContainer,
		textt,
		header,
	} = styles;

	return (
		<View style={styles.container}>
			<View style={header}>
				<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Hello,&nbsp;</Text>
				<Text style={{ fontSize: 18, color: '#ffffff' }}>Senusa Care Here !</Text>
			</View>

			<ScrollView>

				<View style={{ height: 220, paddingHorizontal: 20, elevation: 5, paddingTop: 15 }}>
					<Swiper
						height={200}
						autoplay
						style={{ elevation: 5 }}
						autoplayTimeout={5}
						removeClippedSubviews={false}
						dot={<View style={{ backgroundColor: '#a1a1a1', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
						activeDot={<View style={{ backgroundColor: '#ff0000', width: 8, height: 8, borderRadius: 4, marginLeft: 3, marginRight: 3, marginTop: 3, marginBottom: 3 }} />}
						paginationStyle={{
						bottom: 5, left: null, right: 0
						}} 
						loop
				    >
							<Image
								style={{ width: '100%', height: 170, borderRadius: 5 }}
								source={require('../Assets/img1f.jpg')}
							/>
							<Image
								style={{ width: '100%', height: 170, borderRadius: 5 }}
								source={require('../Assets/img2f.jpg')}
							/>
							<Image
								style={{ width: '100%', height: 170, borderRadius: 5 }}
								source={require('../Assets/img3f.jpg')}
							/>
					</Swiper>
				</View>

			
				<View style={cardContainer}>
					<TouchableOpacity style={card} onPress={() => Actions.homeCareHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/blood.png')}
						/>
						<Text style={textt}>Home Care</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.dokterHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/doctor.png')}
						/>
						<Text style={textt}>Dokter</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.trainingCenterHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/Group.png')}
						/>
						<Text style={textt}>Training Center</Text>
					</TouchableOpacity>
				</View>

				<View style={cardContainer}>
					<TouchableOpacity style={card} onPress={() => Actions.consultant()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/conversation.png')}
						/>
						<Text style={textt}>Consultant</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.storeHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/shopping-bag.png')}
						/>
						<Text style={textt}>Store</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.infoTerkiniHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/newspaper.png')}
						/>
						<Text style={textt}>Info Terkini</Text>
					</TouchableOpacity>
				</View>

				<View style={cardContainer}>
					<TouchableOpacity style={card} onPress={() => Actions.bidanHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/nurse.png')}
						/>
						<Text style={textt}>Bidan</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.labHome()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/flask.png')}
						/>
						<Text style={textt}>Laboraturium</Text>
					</TouchableOpacity>

					<TouchableOpacity style={card} onPress={() => Actions.konsultasiGizi()}>
						<Image
							style={{ width: 50, height: 50, marginBottom: 20 }}
							source={require('../Assets/icon-homepage/frying-pan.png')}
						/>
						<Text style={textt}>Konsultasi Gizi</Text>
					</TouchableOpacity>
				</View>
				<View style={{ marginBottom: 15 }} />
			</ScrollView>
		</View>
	);
};

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 12,
		color: '#231f20'
	},
	card: { 
		width: 100, 
		height: 140, 
		elevation: 5, 
		backgroundColor: '#ffffff', 
		justifyContent: 'center', 
		alignItems: 'center',
		borderRadius: 5,
	},
	cardContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: 20,
		marginTop: 15
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	}
};

export { Homepage };
