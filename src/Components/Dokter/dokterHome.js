/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, Image, Dimensions, TouchableOpacity, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import  { Actions } from 'react-native-router-flux';

const width = Dimensions.get('window').width;

export default class DokterHome extends Component {

	state = {
		dokter: [
			{
				nama: 'Dr. Tri Wahyuni',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: false,
				exp: 6,
				img: require('../../Assets/doc1.jpg')
			},
			{
				nama: 'Dr. Sri Yuliani A',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: true,
				exp: 6,
				img: require('../../Assets/doc3.jpg')
			},
			{
				nama: 'Dr. Aprilia Yusrina',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: false,
				exp: 6,
				img: require('../../Assets/doc2.jpg')
			},
			{
				nama: 'Dr. M. Satrya D.S',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: false,
				exp: 6,
				img: require('../../Assets/doc4.jpg')
			},
			{
				nama: 'Dr. Aprilia Yusrina',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: false,
				exp: 6,
				img: require('../../Assets/doc5.jpg')
			},
			{
				nama: 'Dr. M. Satrya D.S',
				speciality: 'Spesialis Hidung, Paru-Paru, dan Tenggorokan',
				price: 'Rp 70.000',
				free: true,
				exp: 6,
				img: require('../../Assets/doc6.jpeg')
			},

		]
	}
	render() {
		const {
			container,
			header,
			cardContainer,
			orderButton,
			contentContainer,
			inputContainer
		} = styles;

		return (
			<View style={container}>
				<View style={header}>
					<View style={{ flexDirection: 'row', alignItems: 'center' }}>
						<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
              <Icon 
                name="angle-left"
                size={30}
                color="#ffffff"
              />
            </TouchableOpacity>
						<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Dokter</Text>
					</View>

					<View 
						style={inputContainer}
					>
						<Icon 
              name="search"
              size={20}
              style={{ marginRight: 25 }}
              color="#ffffff"
            />
						<TextInput
							placeholder='Cari Berdasarkan nama Dokter'
							placeholderTextColor='#ffffff'
							style={{ color: '#ffffff' }}
							underlineColorAndroid="transparent"
						/>
					</View>
				</View>

				<View style={contentContainer}>
					<FlatList
						data={this.state.dokter}
						numColumns={1}
						contentContainerStyle={{ paddingTop: 20 }}
						showsVerticalScrollIndicator={false}
						keyExtractor={(item, index) => index.toString()}
						renderItem={({ item, index }) =>
							<TouchableOpacity style={cardContainer} onPress={() => Actions.profilDokter({ names: item.nama, img: item.img, speciality: item.speciality })}>
								<Image
									style={{ height: '100%', width: 110, borderRadius: 5, marginRight: 10 }}
									source={item.img}
								/>
								<View style={{ flex: 1, justifyContent: 'space-between' }}>
									<Text style={{ fontSize: 14, color: '#231f20', fontWeight: '900' }}>{item.nama}</Text>
									<Text style={{ fontSize: 12, color: '#231f20' }}>{item.speciality}</Text>
									<Text style={{ fontSize: 10, color: '#231f20' }}>{item.exp} Tahun Pengalaman</Text>

									<View style={{ borderTopWidth: 1, width: '100%', borderColor: '#231f20' }} />

									<View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
										<View style={{ justifyContent: 'space-between' }}>
											{ 
												item.free ? 
												<Text style={{ fontSize: 10, color: '#231f20', textDecorationLine: 'line-through' }}>{item.price}</Text> 
												:
												<Text style={{ fontSize: 10, color: '#231f20', textDecorationLine: 'line-through' }}></Text> 	
											}

											{ 
												item.free ? 
												<Text style={{ fontSize: 14, color: 'skyblue', fontWeight: '900' }}>FREE</Text> 
												:
												<Text style={{ fontSize: 14, color: 'skyblue', fontWeight: '900' }}>Rp 70.000</Text> 
											}
										</View>

										<TouchableOpacity style={orderButton} onPress={() => Actions.dokterChat({ namaDokter: item.nama })}>
											<Text style={{ fontSize: 12, color: '#ffffff', fontWeight: '900' }}>PESAN</Text>
										</TouchableOpacity>
									</View>
								</View>

							</TouchableOpacity>
						}
					/>
				</View>

						
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		//justifyContent: 'center',
	},
	header: {
		height: 180,
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE',
		justifyContent: 'space-between',
		paddingTop: 40,
		paddingBottom: 30
	},
	orderButton: { 
		alignItems: 'center', 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 20, 
		borderRadius: 3 
	},
	cardContainer: { 
		width: width - 40,
		flexDirection: 'row',
		alignSelf: 'center',
		height: 180,
		marginBottom: 20,
		padding: 10,
		borderRadius: 5,
		backgroundColor: '#ffffff',
		elevation: 5,
	},
	contentContainer: {
		flex: 1,
	},
	inputContainer: { 
		alignItems: 'center', 
		height: 50, 
		flexDirection: 'row', 
		width: '100%', 
		backgroundColor: '#ABD2FC', 
		borderRadius: 5, 
		paddingHorizontal: 15 
	}

});
