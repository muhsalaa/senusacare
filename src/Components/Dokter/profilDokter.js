import React, { Component } from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback, TextInput } from 'react-native';
import { Actions } from 'react-native-router-flux';
import ModalDropdown from 'react-native-modal-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class profilDokter extends Component {
	state = {
		cash: true,
		transfer: false,
		voucher: false,
		service: [
			'Jantung',
			'Hati',
			'Otak',
			'Tulang',
			'gigi',
			'Telinga',
			'Pikiran',
			'Kaki',
			'Pencernaan'
		],
		serviceChoosen: null,
	}

	render() {
		const {
			header,
			textt,
			texttt,
			textt2,
			card,
			totalTextt,
			orderButton,
			dotInactive,
			promoCode,
			dotActive,
			imageContainer,
			contianerTop,
			dropDownContainer
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<Image
						style={{ width: '100%', height: 130 }}
						source={require('../../Assets/img1.jpg')}
					/>
					<TouchableOpacity style={{ position: 'absolute', left: 20, top: 25 }} onPress={() => Actions.pop()}>
						<Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
						/>
					</TouchableOpacity>
				</View>

				{
          // photos and open sign
        }
        <View style={contianerTop}>
          <View style={imageContainer}>
            <Image 
              style={{ width: 120, height: 120, borderRadius: 60 }} 
              source={this.props.img}
            />
          </View>
          {
            // open sign
          }
          <View style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 14, fontWeight: '600', color: '#231f20' }}>{this.props.names}</Text>
            <Text style={{ fontSize: 12, color: '#231f20' }}>{this.props.speciality}</Text>
          </View> 
        </View>

				<ScrollView style={{ marginTop: 125 }}>
				<View style={{ flex: 1 }}>
					{
						//Second card, Detail Pembayaran
					}
					<View style={card}>
						<Text style={texttt}>Konsultasi Untuk</Text>
						<View style={dropDownContainer}>
							<ModalDropdown 
								options={this.state.service}
								style={{ flex: 1 }}
								dropdownStyle={{ width: width - 70, height: 160 }}
								dropdownTextStyle={{ color: '#231f20', fontSize: 12 }}
								textStyle={{ color: '#808080', fontSize: 12 }}
								animated
								defaultValue={this.state.service[0]}
								onSelect={(values) => this.setState({ serviceChoosen: values })}
							/>
							<Icon 
								name="caret-down"
								size={18}
								color="#231f20"
							/>
						</View>
						<Text style={textt2}>Detail Pembayaran</Text>
					
						<TouchableWithoutFeedback onPress={() => this.setState({ cash: true, transfer: false })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.cash && dotActive]} />
								<Text style={textt}>Bayar Tunai Ditempat</Text>
							</View>
						</TouchableWithoutFeedback>


						<TouchableWithoutFeedback onPress={() => this.setState({ cash: false, transfer: true })}>
							<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
								<View style={[dotInactive, this.state.transfer && dotActive]} />
								<Text style={textt}>Bank Transfer</Text>
							</View>
						</TouchableWithoutFeedback>

						<View style={{ flexDirection: 'row', marginLeft: 24, marginBottom: 15 }}>
							<Image
								style={{ height: 40, width: 80, marginRight: 10 }}
								source={require('../../Assets/icon-another/logo-bni.png')}
							/>

							<View style={{ flex: 1 }}>
								<Text style={{ fontSize: 12, color: '#231f20', lineHeight: 12 }}>Transfer Ke:</Text> 
								<Text style={{ fontSize: 12, color: '#7AB4FE' }}>0741325741</Text> 
								<Text style={{ fontSize: 10, color: '#231f20' }}>A/n yayasan sosial teknologi nusantara</Text> 
							</View>
						</View>

						<View style={totalTextt}>
							<Text style={texttt}>Total Pembayaran</Text>
							<Text style={{ ...texttt, fontWeight: '500' }}>Rp 300.022</Text>
						</View>
					</View>
					{
						// Last Card Detail Pemesanan
					}
					<View style={card}>
							<TouchableWithoutFeedback onPress={() => this.setState({ voucher: !this.state.voucher })}>
								<View style={{ flexDirection: 'row', marginBottom: 15, alignItems: 'center' }}>
									<View style={[dotInactive, this.state.voucher && dotActive]} />
									<Text style={{ color: '#231f20', fontSize: 10, fontWeight: '600' }}>GUNAKAN VOUCHER GRATIS KONSULTASI</Text>
								</View>
							</TouchableWithoutFeedback>

							<TextInput
								style={promoCode}
							/>
					</View>

				</View>

				<TouchableOpacity style={orderButton}>
					<Text style={{ fontSize: 14, color: '#ffffff', fontWeight: '500' }}>BAYAR</Text>
				</TouchableOpacity>
				</ScrollView>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: { 
		fontSize: 12, 
		color: '#231f20', 
		flex: 1 
	},
	texttt: { 
		fontSize: 12, 
		color: '#231f20', 
	},
	textt2: { 
		fontSize: 14, 
		fontWeight: '900', 
		color: '#231f20',
		marginBottom: 15 
	},
	header: {
		height: 130,
		flexDirection: 'row',
		alignItems: 'center',
		backgroundColor: '#7AB4FE'
	},
	card: { 
		padding: 15,
		width: width - 40,
		alignSelf: 'center',
		elevation: 5, 
		backgroundColor: '#ffffff',
		borderRadius: 5,
		marginBottom: 15,
	},
	totalTextt: { 
		flexDirection: 'row', 
		marginBottom: 15, 
		justifyContent: 'space-between',
		marginLeft: 24
	},
	orderButton: { 
		alignItems: 'center',
		width: width - 40, 
		justifyContent: 'center', 
		backgroundColor: '#F88C8F', 
		paddingHorizontal: 30,
		paddingVertical: 10,
		borderRadius: 5,
		alignSelf: 'center',
		marginBottom: 15
	},
	dotInactive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10
	},
	dotActive: { 
		height: 14, 
		width: 14, 
		borderRadius: 7,
		borderWidth: 1,
		borderColor: '#F88C8F',
		marginRight: 10,
		backgroundColor: '#F88C8F'
	},
	promoCode: {
		flex: 1,
		height: 40,
		backgroundColor: '#e9e9e9',
		color: '#231f20',
		borderRadius: 5,
		paddingHorizontal: 10,
		fontSize: 12,
		marginLeft: 24
	},
	contianerTop: { 
		alignItems: 'center', 
		position: 'absolute', 
		top: 65, 
		left: 0, 
		right: 0 
	},
  imageContainer: { 
    width: 124, 
    height: 124, 
    borderRadius: 62, 
    backgroundColor: '#ffffff', 
    justifyContent: 'center', 
    alignItems: 'center',
    marginBottom: 10
  },
  dropDownContainer: { 
		alignItems: 'center', 
		flexDirection: 'row', 
		width: '100%',
		height: 40,
		borderRadius: 5,
		paddingHorizontal: 10,
		paddingVertical: 12,
		backgroundColor: '#e9e9e9',
		marginTop: 5,
		marginBottom: 10
	},
};
