import React, { Component } from 'react';
import { View, Text, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

const width = Dimensions.get('window').width;

export default class StoreHome extends Component {
	
	render() {
		const {
			header,
			button,
			buttonContainer,
			buttonLast,
			textt
		} = styles;

		return (
			<View style={styles.container}>
				<View style={header}>
					<TouchableOpacity style={{ marginRight: 30 }} onPress={() => Actions.pop()}>
					  <Icon 
						name="angle-left"
						size={30}
						color="#ffffff"
					  />
					</TouchableOpacity>
					<Text style={{ fontSize: 18, fontWeight: '900', color: '#ffffff' }}>Store</Text>
				</View>

				<ScrollView>
					<View style={{ flex: 1, paddingTop: 15 }}>
						{
							//1
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/cough.png')}
								/>
								<Text style={textt}>Batuk & Flu</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/drug.png')}
								/>
								<Text style={textt}>Vitamin & Suplement</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/like.png')}
								/>
								<Text style={textt}>Jantung</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/electric-meter.png')}
								/>
								<Text style={textt}>Hipertensi</Text>
							</TouchableOpacity>
						</View>
						{
							//2
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/oxygen-mask.png')}
								/>
								<Text style={textt}>Asma</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/sickk.png')}
								/>
								<Text style={textt}>Demam</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/epidermis.png')}
								/>
								<Text style={textt}>Kulit</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/consumer.png')}
								/>
								<Text style={textt}>Produk Konsumen</Text>
							</TouchableOpacity>
						</View>
						{
							//3
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/mother.png')}
								/>
								<Text style={textt}>Ibu & Anak</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/hospital.png')}
								/>
								<Text style={textt}>P3K</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/woman.png')}
								/>
								<Text style={textt}>Wanita</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/condoms.png')}
								/>
								<Text style={textt}>Kontrasepsi</Text>
							</TouchableOpacity>
						</View>

						{
							//4
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/stomach.png')}
								/>
								<Text style={textt}>Maag & Lambung</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/knee.png')}
								/>
								<Text style={textt}>Anti Nyeri</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/sugar-blood-level.png')}
								/>
								<Text style={textt}>Diabetes</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/medical-kit.png')}
								/>
								<Text style={textt}>Alat Kesehatan</Text>
							</TouchableOpacity>
						</View>
						{
							//5
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/strong.png')}
								/>
								<Text style={textt}>Stamina</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/supplement.png')}
								/>
								<Text style={textt}>Antibiotik</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/herbal.png')}
								/>
								<Text style={textt}>Herbal</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/fish.png')}
								/>
								<Text style={textt}>Diet</Text>
							</TouchableOpacity>
						</View>
						{
							//6
						}
						<View style={buttonContainer}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/intestine.png')}
								/>
								<Text style={textt}>Diare</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/allergy.png')}
								/>
								<Text style={textt}>Alergi</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/visibility.png')}
								/>
								<Text style={textt}>Mata</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/bones.png')}
								/>
								<Text style={textt}>Obat Tulang & Sendi</Text>
							</TouchableOpacity>
						</View>
						{
							//7
						}
						<View style={buttonLast}>
							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/antiseptic.png')}
								/>
								<Text style={textt}>Antiseptik</Text>
							</TouchableOpacity>

							<TouchableOpacity style={button} onPress={() => Actions.katalogBarang()}>
								<Image
									style={{ width: 30, height: 30 }}
									source={require('../../Assets/icon-store/play-button.png')}
								/>
								<Text style={textt}>Lainya</Text>
							</TouchableOpacity>
						</View>

					</View>
				</ScrollView>
		
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
	},
	textt: {
		fontSize: 8,
		color: '#231f20',
		textAlign: 'center'
	},
	button: { 
		width: 70, 
		height: 100, 
		backgroundColor: '#ffffff',
		elevation: 3,
		borderRadius: 5,
		justifyContent: 'space-between',
		alignItems: 'center',
		paddingTop: 25,
		paddingBottom: 5,
	},
	buttonContainer: { 
		flexDirection: 'row', 
		justifyContent: 'space-between', 
		paddingHorizontal: 15, 
		paddingBottom: 15
	},
	buttonLast: { 
		flexDirection: 'row', 
		paddingHorizontal: 15, 
		paddingBottom: 15,
		justifyContent: 'space-between',
		width: (width + 15) / 2
	},
	header: {
		height: 80,
		flexDirection: 'row',
		alignItems: 'center',
		paddingHorizontal: 20,
		backgroundColor: '#7AB4FE'
	}
};
